let w = 50;

let h = 50;

let target = document.getElementById('box');

grow();

function grow() {
    let growInterval = setInterval(function () {
        if (w >= 500) {
            clearInterval(growInterval);
            shrink(); 
        }
        w += 5;
        h += 5;
        target.style.width = w + 'px';
        target.style.height = h + 'px';
        target.style.background = getRandomColor();
    }, 500);
}

function shrink() {
    let shrinkInterval = setInterval(function () {
        if (w <= 50) {
            clearInterval(shrinkInterval);
            grow(); 
        }
        w -= 5;
        h -= 5;
        target.style.width = w + 'px';
        target.style.height = h + 'px';
        target.style.background = getRandomColor();
    }, 500);
}

function getRandomColor () {
    let red = Math.floor(Math.random() * 256);
    let green = Math.floor(Math.random() * 256);
    let blue = Math.floor(Math.random() * 256);
    return 'rgb(' + red + ',' + green + ',' + blue + ')';
}
