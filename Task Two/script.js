
let currentImage = 1;
let imageSlides = document.getElementsByClassName('slide');
let autoPlayInterval;
let randomPlayInterval;

displayImg(currentImage);

function nextimg (next) {
    displayImg( currentImage += next);
}

function currentSlide (next) {
    displayImg(currentImage = next);
}

function displayImg(next) {
    if(next > imageSlides.length){
        currentImage = 1;
    }

    if(next < 1){
        currentImage = imageSlides.length;
    }
    for(i=0 ; i < imageSlides.length ; i++){
        imageSlides[i].style.display = "none";
    }

    imageSlides[currentImage - 1].style.display = "block"
}

function startAutoPlay() {
     autoPlayInterval = setInterval(() => {
        nextimg(1);
    }, 2000); 
}

function stopAutoPlay (){
    clearInterval(autoPlayInterval);
}

function randomSlide (){
    randomPlayInterval = setInterval(() => {
        let randomIndex = Math.floor(Math.random() * imageSlides.length);
        currentSlide(randomIndex);
    }, 2000);
}

function stopRandomAutoPlay() {
    clearInterval(randomPlayInterval);
}